import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;


public class test {


	@SuppressWarnings("deprecation")
	@Test
	public void testDebit() {
		
		
		Compte c1 = new Compte(500);
		Compte c2 = new Compte(200);
		
		c1.Debit(300);
		System.out.println(c1.getSolde());
		Assert.assertEquals(200.0, c1.getSolde());
		
		
		//fail("Not yet implemented");
	}
	
	
	@SuppressWarnings("deprecation")
	@Test
	public void testCredit() {
		
		
		Compte c1 = new Compte(500);
		Compte c2 = new Compte(200);
		
		c1.Credit(-300);
		System.out.println(c1.getSolde());
		Assert.assertEquals(200.0, c1.getSolde());
		
		
		//fail("Not yet implemented");
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testVirement() {
		
		
		Compte c1 = new Compte(500);
		Compte c2 = new Compte(200);
		
		c1.Virement(c2, 600);
		System.out.println(c2.getSolde());
		Assert.assertEquals(800.0, c2.getSolde());
		
		
		//fail("Not yet implemented");
	}

}
